package cn.xinyi.com.formvalidation.myvalidation;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import cn.xinyi.com.formvalidation.lib.ValidationExecutor;

/**
 * Created by Administrator on 2016/3/11 0011.
 */
public class Edit2Validation extends ValidationExecutor{
    //假如EditText必须要输入456
    @Override
    public boolean doValidate(Context context, String text) {
        if(extraParams != null){
            for(int i = 0; i < extraParams.length;i ++){
                Log.i("cz", (String) extraParams[i]);
            }
        }

        if(!TextUtils.equals("456",text)){
            Toast.makeText(context, "must be 456", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
