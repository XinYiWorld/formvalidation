package cn.xinyi.com.formvalidation;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cn.xinyi.com.formvalidation.lib.EditTextValidator;
import cn.xinyi.com.formvalidation.lib.ValidationModel;
import cn.xinyi.com.formvalidation.myvalidation.Edit1Validation;
import cn.xinyi.com.formvalidation.myvalidation.Edit2Validation;

public class MainActivity extends Activity implements View.OnClickListener {

    private EditTextValidator registValidator;
    private Edit1Validation edit1ValidationExecutor;
    private Edit2Validation edit2ValidationExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        EditText et1 = (EditText) findViewById(R.id.et1);
        EditText et2 = (EditText) findViewById(R.id.et2);
        View btn_submit = findViewById(R.id.submit);
        btn_submit.setOnClickListener(this);
        //注册表单验证框架，将各个选项的校验封装在校验器里。
        edit1ValidationExecutor = new Edit1Validation();
        edit2ValidationExecutor = new Edit2Validation();

        edit1ValidationExecutor.setExtraParams("edit1P1","edit1P2");
        edit2ValidationExecutor.setExtraParams("edit2P1","edit2P2");

        registValidator = new EditTextValidator(this)
                .setButton(btn_submit)
                .add(new ValidationModel(et1, edit1ValidationExecutor))
                .add(new ValidationModel(et2, edit2ValidationExecutor))
                .execute();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.submit){
            if(registValidator.validate()){
                Toast.makeText(this,"表单验证通过",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
